import React, { Component } from 'react';
import './Toggle.css';

class Toggle extends Component {
    constructor(props) {
        super(props);

        this.state = {
            on: this.props.default || false
        }
    }

    handleClick = () => {
        this.setState(prevState => {
            return {
                on: !prevState.on
            };
        }, () => {
            if (this.props.onChange)
                this.props.onChange(this.state.on);
        })
    }

    getCaption = () => {
        return this.state.on
            ? this.props.onCaption
            : this.props.offCaption;
    }

    render() {
        return (
            <div className={"Toggle " + (this.state.on ? "On" : "Off")} onClick={this.handleClick} >
                <div className="Thumb" />
                <div className="Separator" />
                <span className="Caption">{this.getCaption()}</span>
            </div>
        );
    }
}

export default Toggle;