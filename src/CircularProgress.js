import React, { Component } from 'react';
import './CircularProgress.css';

class CircularProgress extends Component {
    render() {
        const defaultSize = "40px";

        return (
            <div className="CircularProgress" style={{ width: this.props.size || defaultSize, height: this.props.size || defaultSize }}/>
        );
    }
}

export default CircularProgress;