const API_KEY = "7aad90313567a3833763115494814ac4";     // raplace before publishing code
const API_URI = "http://api.openweathermap.org";
const API_URI_WEATHER_SUFFIX = "/data/2.5/weather";
const API_URI_FORECAST_SUFFIX = "/data/2.5/forecast";

function sendRequest(URI, onSuccess, onError) {
    let request = new XMLHttpRequest();

    request.onreadystatechange = function() {
        if (this.readyState === 4)
            if (this.status === 200) {
                let response = JSON.parse(this.responseText);
                onSuccess(response);
            }
            else
                onError(this.status);
    }

    request.open("GET", URI, true);
    request.send();
}

class OWMUtils {
    // weather for today
    getWeather = (city, lat, lon, onSuccess, onError) => {
        const URI = API_URI + API_URI_WEATHER_SUFFIX
            + "?APPID=" + API_KEY
            + (city ? ("&q=" + city) : ("&lat=" + lat + "&lon=" + lon));

        sendRequest(URI, onSuccess, onError);
    }

    // forecast for 5 days
    getForecast = (city, lat, lon, onSuccess, onError) => {
        const URI = API_URI + API_URI_FORECAST_SUFFIX
            + "?APPID=" + API_KEY
            + (city ? ("&q=" + city) : ("&lat=" + lat + "&lon=" + lon));

        sendRequest(URI, onSuccess, onError);
    }

    // response returns 40* time intervals spaced by 3h, but we want to display only 1 per day
    // starting 24h from now and then each consecutive 24h
    // * response is actually not guaranteed to return 40 intervals, therefore we traverse the list 
    //   backwards to get the most spaced results from "now"
    purgeForecastResponse = (resp) => {
        const INTERVALS_IN_A_DAY = 24 / 3;

        let results = [];
        for (let i = (resp.list.length - 1); i > 0; i -= INTERVALS_IN_A_DAY) {
            results.unshift(resp.list[i]);
        }

        return results;
    }
}

export default new OWMUtils();