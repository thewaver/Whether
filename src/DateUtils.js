const WEEKDAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

const domEnder = (day) => {
    const endsWith = (str, suffix) => {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    if (endsWith(day, "11") || endsWith(day, "12") || endsWith(day, "13"))
        return "th";

    const n = day.charAt(day.length - 1);
    return "1" === n ? "st" : "2" === n ? "nd" : "3" === n ? "rd" : "th";
}

class DateUtils {
    getWeekdayAsString = (date) => {
        return WEEKDAYS[date.getDay()];
    }

    getDayOfMonthAsString = (date) => {
        return date.getDate() + domEnder(date.getDate().toString());
    }

    getMonthAsString = (date) => {
        return MONTHS[date.getMonth()];
    }

    getDateAsString = (date) => {
        const dayOfWeek = this.getWeekdayAsString(date);
        const dayOfMonth = this.getDayOfMonthAsString(date);
        const curMonth = this.getMonthAsString(date);
        const curYear = date.getFullYear();

        return dayOfWeek + ", " + curMonth + " " + dayOfMonth + " " + curYear;
    }
}

export default new DateUtils();