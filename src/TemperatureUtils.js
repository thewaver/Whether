const UNITS = { 
    celsius: {
        symbol: 'ºC',
        fromKelvin: (kelvin) => (kelvin - 273.15).toFixed(0)
    },
    fahrenheit: {
        symbol: 'ºF',
        fromKelvin: (kelvin) => (kelvin * 9 / 5 - 459.67).toFixed(0)
    },
};

class TemperatureUtils {
    getTemperatureUnits = () => {
        return UNITS;
    }

    getTemperatureUnitKeyFromSymbol = (symbol) => {
        for (const [key, value] of Object.entries(UNITS)) {
            if (value.symbol === symbol)
                return key;
        }
    }
}

export default new TemperatureUtils();