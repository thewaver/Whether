export const ERRORS = {
    myLocationFailed: "Failed to retrieve location.",
    geolocationUnavailable: "Geolocation is not enabled or supported by your browser.",
    forecastNotRetrieved: "Could not retrieve weekly forecast information.",
    cityNotFound: "Could not find the specified city."
}

export const LABELS = {
    city: "city",
    morning: "morning",
    day: "day",
    evening: "evening",
    night: "night"
}

export const COOKIE_NAMES = {
    city: "city",
    unitSymbol: "unitSymbol"
}