import React, { Component } from 'react';
import CircularProgress from './CircularProgress';
import LocationSelector from './LocationSelector';
import WeatherView from './WeatherView';
import OWMUtils from './OWMUtils';
import CookieUtils from './CookieUtils';
import { ERRORS, COOKIE_NAMES } from './AppStrings';
import './Panel.css';

// yes, Panel is a terribly generic name - I would never use it in a large project
// with multiple "panels", but I didn't want to spend a lot of time thinking about a better name
// and given the miniscule scope of this project, it's hard to know which components should be
// implemented in a more specialized or abstracted manner
class Panel extends Component {
    constructor(props) {
        super(props);

        this.panes = {
            locationSelection: this.renderLocationSelection,
            weatherView: this.renderWeatherView,
            progress: this.renderProgress
        };

        let city = CookieUtils.getCookie(COOKIE_NAMES.city);

        this.state = {
            pane: city ? this.panes.weatherView : this.panes.locationSelection,
            city: city || ""
        };
    }

    componentDidMount() {
        if (this.state.city)
            this.handleCityConfirmation(this.state.city, null, null);
    }

    handleCityConfirmation = (city, lat, lon) => {
        const commonState = {
            pane: this.panes.weatherView,
            city: city
        }

        this.setState({
            pane: this.panes.progress,
            errorMessage: ""
        }, () => {
            OWMUtils.getWeather(city, lat, lon,
                wresponse => {
                    CookieUtils.setCookie(COOKIE_NAMES.city, city || wresponse.name);

                    OWMUtils.getForecast(city, lat, lon,                    
                        fresponse => {
                            const pfresponse = OWMUtils.purgeForecastResponse(fresponse);
                            this.setState({ ...commonState, ...{
                                weather: wresponse,
                                forecast: pfresponse
                            }});
                        },
                        fstatus => {
                            this.setState({ ...commonState, ...{
                                weather: wresponse,
                                errorMessage: ERRORS.forecastNotRetrieved
                            }});
                        }
                    );
                },
                wstatus => {
                    this.setState({
                        pane: this.panes.locationSelection,
                        errorMessage: ERRORS.cityNotFound
                    });
                }
            );
        });
    }

    handleWeatherViewReturn = () => {
        this.setState({
            pane: this.panes.locationSelection
        });
    }

    setErrorMessage = (mesg) => {
        this.setState({
            errorMessage: mesg
        });
    };

    renderLocationSelection = () => {
        return (
            <LocationSelector onConfirm={this.handleCityConfirmation}/>
        );
    }

    renderWeatherView = () => {
        return (
            <WeatherView onReturn={this.handleWeatherViewReturn} city={this.state.city} todayWeather={this.state.weather} forecast={this.state.forecast} />
        );
    }

    renderProgress = () => {
        return (
            <CircularProgress size="80px" />
        );
    }

    renderErrorMessage = () => {
        if (this.state.errorMessage && this.state.errorMessage !== "")
            return (
                <div className="ErrorMessageDiv">
                    {this.state.errorMessage}
                </div>
            );
    }

    render() {
        return (
            <div className="Panel">
                {this.state.pane()}
                {this.renderErrorMessage()}
            </div>
        );
    }
}

export default Panel;