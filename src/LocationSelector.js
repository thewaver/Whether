import React, { Component } from 'react';
import { ERRORS, LABELS } from './AppStrings';
import './LocationSelector.css';

class LocationSelector extends Component {
    constructor(props) {
        super(props);

        this.state = {
            city: ""
        }
    }

    handleCityChange = (e) => {
        this.setState({
            city: e.target.value
        });
    }

    handleMyLocationClick = (e) => {
        e.preventDefault();
        e.stopPropagation();

        if (navigator.geolocation)
            navigator.geolocation.getCurrentPosition((position) => {
                this.props.onConfirm(null, position.coords.latitude, position.coords.longitude);
            }, () => {
                const errorMesg = ERRORS.myLocationFailed;

                if (this.props.onError)
                    this.props.onError(errorMesg);
                else
                    alert(errorMesg);
            });
        else {
            const errorMesg = ERRORS.geolocationUnavailable;

            if (this.props.onError)
                this.props.onError(errorMesg);
            else
                alert(errorMesg);
        }
    }

    handleCityClick = () => {
        if (this.state.city && this.state.city !== "")
            this.props.onConfirm(this.state.city, null, null);
    }

    handleCityKeyDown = (e) => {
        if (e.keyCode === 13)
            this.handleCityClick();
    }

    render() {
        // MyLocation content is not abstracted away as a const because replacing the link properties
        // at runtime would require an advanced ICU-compliant string parser and I don't feel like
        // implementing one as part of this task just for the effect
        return (
            <div className="LocationSelector">
                <div className="LocationInputDiv">
                    <input value={this.state.city} placeholder={LABELS.city} onChange={this.handleCityChange} onKeyDown={this.handleCityKeyDown} />
                    <button onClick={this.handleCityClick} disabled={this.state.city ===""}>
                        <i className="material-icons">search</i>
                    </button>
                </div>
                <span className="LonelyOr">or</span>
                <span className="MyLocation">use my <a href="" onClick={this.handleMyLocationClick}>current position</a></span>
            </div>
        );
    }
}

export default LocationSelector;