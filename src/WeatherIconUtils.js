// utility for integration with https://erikflowers.github.io/weather-icons/

class WheatherIconUtils {
    getIconPrefix(sunrise, sunset) {
        const currentTime = new Date().getTime();

        if (currentTime >= sunrise && currentTime <= sunset)
            return "day-";
        else
            return "night-";
    }

    getNewtralIconClassName(id) {
        return "wi wi-owm-" + id;
    }

    getCurrentIconClassName(id, sunrise, sunset) {
        return  "wi wi-owm-" + this.getIconPrefix(sunrise, sunset) + id;
    }
}

export default new WheatherIconUtils();