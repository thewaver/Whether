import React, { Component } from 'react';
import Toggle from './Toggle';
import CookieUtils from './CookieUtils';
import DateUtils from './DateUtils';
import TemperatureUtils from './TemperatureUtils';
import WeatherIconUtils from './WeatherIconUtils';
import { LABELS, COOKIE_NAMES } from './AppStrings';
import './WeatherView.css';

class SmallWeatherItem extends Component {
    getDayAsString = () => {
        return DateUtils.getWeekdayAsString(new Date(this.props.item.dt * 1000));
    }

    getTemperatureString = () => {
        return this.props.unit.fromKelvin(this.props.item.main.temp).toString() + this.props.unit.symbol;
    }

    getIconClassName = () => {
        return WeatherIconUtils.getNewtralIconClassName(this.props.item.weather[0].id) + " Icon";
    }

    render() {
        return (
            <div className="SmallWeatherItem">
                <span className="WeekdayTitle">{this.getDayAsString()}</span>
                <i className={this.getIconClassName()} />
                <span className="Temperature">{this.getTemperatureString()}</span>
            </div>
        );
    }
}

class WeatherView extends Component {
    constructor(props) {
        super(props);

        const unitSymbol = CookieUtils.getCookie(COOKIE_NAMES.unitSymbol);        
        const unitKey = unitSymbol ? TemperatureUtils.getTemperatureUnitKeyFromSymbol(unitSymbol) : null;
        const units = TemperatureUtils.getTemperatureUnits();

        this.defaultUnit = units.celsius;

        this.state = {
            unit: unitKey ? units[unitKey] : this.defaultUnit
        }
    }

    getCityName = () => {
        return this.props.city || (this.props.todayWeather && this.props.todayWeather.name);
    }

    getDateAsString = () => {
        if (this.props.todayWeather)
            return DateUtils.getDateAsString(new Date(this.props.todayWeather.dt * 1000));
    }

    getTodayCondition = () => {
        if (this.props.todayWeather)
            return this.props.todayWeather.weather[0].description;
    }

    getTodayTemperatureAsString = () => {
        if (this.props.todayWeather)
            return this.state.unit.fromKelvin(this.props.todayWeather.main.temp) + this.state.unit.symbol;
    }

    getTodayIconClassName = () => {
        if (this.props.todayWeather) {
            const sunrise = new Date(this.props.todayWeather.sys.sunrise * 1000).getTime();
            const sunset = new Date(this.props.todayWeather.sys.sunset * 1000).getTime();
            const className = WeatherIconUtils.getCurrentIconClassName(this.props.todayWeather.weather[0].id, sunrise, sunset) + " LargeIcon";

            return className;
        }
    }

    handleUnitChange = (on) => {
        const units = TemperatureUtils.getTemperatureUnits();

        this.setState({
            unit: on ? units.celsius : units.fahrenheit
        }, () => {
            CookieUtils.setCookie(COOKIE_NAMES.unitSymbol, this.state.unit.symbol);
        });
    }

    renderForecast = () => {
        if (this.props.forecast)
            return (
                <div className="Footer">
                    {
                        this.props.forecast.map((item, index) =>
                            <SmallWeatherItem key={index} item={item} unit={this.state.unit} />
                        )
                    }
                </div>
            );
    }

    render() {
        const units = TemperatureUtils.getTemperatureUnits();

        return (
            <div className="WeatherView">
                <div className="Header">
                    <button onClick={this.props.onReturn}>
                        <i className="material-icons">arrow_back</i>
                    </button>
                    <span className="CityName">{this.getCityName()}</span>
                    <Toggle onCaption={units.celsius.symbol} offCaption={units.fahrenheit.symbol} default={this.state.unit === this.defaultUnit} onChange={this.handleUnitChange} />
                </div>
                <div className="Content">
                    <span className="TodayDate">{this.getDateAsString()}</span>
                    <span className="TodayCondition">{this.getTodayCondition()}</span>
                    <div className="TodayInfo">
                        <span className="Temperature">{this.getTodayTemperatureAsString()}</span>
                        <i className={this.getTodayIconClassName()} />
                        <div className="Details">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>{LABELS.morning}</td>
                                        <td className="DetailTemperature">{"???"}</td>
                                    </tr>
                                    <tr>
                                        <td>{LABELS.day}</td>
                                        <td className="DetailTemperature">{"???"}</td>
                                    </tr>
                                    <tr>
                                        <td>{LABELS.evening}</td>
                                        <td className="DetailTemperature">{"???"}</td>
                                    </tr>
                                    <tr>
                                        <td>{LABELS.night}</td>
                                        <td className="DetailTemperature">{"???"}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {this.renderForecast()}
            </div>
        );
    }
}

export default WeatherView;