import React, { Component } from 'react';
import Panel from './Panel'
import './App.css';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Panel />
            </div>
        );
    }
}

export default App;
